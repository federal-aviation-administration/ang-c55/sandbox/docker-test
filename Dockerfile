FROM alpine:3.14.2

LABEL org.opencontainers.image.description="Experimenting with docker on gitlab.com" \
      org.opencontainers.image.licenses="CC0-1.0" \ 
      org.opencontainers.image.vendor="US DOT, FAA, Office of NextGen, Modeling and Simulation Branch" \       
      org.label-schema.license="CC0-1.0" \
      org.label-schema.schema-version=1.0.0-rc.1 \
      org.label-schema.vendor="US DOT, FAA, Office of NextGen, Modeling and Simulation Branch"

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates
